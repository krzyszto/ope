all: ope

ope: ope.cpp
	g++ -Wall -o ope ope.cpp -g

clean:
	rm ope

remake:
	make clean
	make all

.PHONY: clean all
