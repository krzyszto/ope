#include <stdio.h>
#include <algorithm>
#include <vector>
#include <utility>
#include <limits.h>

using namespace std;
int x, T = 0;
int M;
typedef vector< pair<int, int> > przedzialowe; //czas, wartosc


int potega_dwojki(const int a)
{
	int pocz = 1;
	while (pocz < a + 1)
		pocz *= 2;
	return pocz;
}

void usun(przedzialowe *drzewo)
{
	delete[] drzewo;
}

void wstaw(const int pocz, const int kon, const int ile, const int T,
							przedzialowe *drzewo)
{
	//printf("pocz to %i, kon to %i, ile to %i\n", pocz, kon, ile);
	int va = M + pocz, vb = M + kon;
	pair <int, int> tmp2 = std::make_pair(T, drzewo[va].back().second + ile);
	//printf("wstawiamy do %i wezla\n", va);
	/* Skrajne przedziały do rozkładu. */
	drzewo[va].push_back(tmp2);
	if (va != vb) {
		pair <int, int> tmp = std::make_pair(T, drzewo[vb].back().second + ile);
		drzewo[vb].push_back(tmp);
		//printf("wstawiamy do %i wezla\n", vb);
	}
	/* Spacer aż do momentu spotkania. */
	while (va / 2 != vb / 2) {
		if (va % 2 == 0) {
			pair <int, int> tmp = std::make_pair(T, drzewo[va + 1].back().second + ile);
			drzewo[va + 1].push_back(tmp);
			//printf("puszujemy pare %i %i do wezla %i\n", tmp.first, tmp.second, va+1);
					/* prawa bombka na lewej ścieżce */
		}
		if (vb % 2 == 1) {
			pair <int, int> tmp = std::make_pair(T, drzewo[vb - 1].back().second + ile);
			drzewo[vb - 1].push_back(tmp);
			//printf("puszujemy pare %i %i do wezla %i\n", tmp.first, tmp.second, vb-1);
					/* lewa bombka na prawej ścieżce */
		}
		//printf("wstawilim do %i i %i wezla\n", va, vb);
		va /= 2; vb /= 2;
	}
}

int ile(const int T, const int i0, przedzialowe *drzewo)
{
	int wynik = 0;
	for (int v = i0 + M; v > 0; v /= 2) {
		vector<pair<int, int> >::iterator tmp;
		tmp = lower_bound(drzewo[v].begin(),
					drzewo[v].end(), make_pair(T, 0));\
		if (tmp == drzewo[v].end()) {
			tmp--;
		}
		while (tmp->first > T) {
			tmp--;
		}
		wynik += tmp->second;
		//if (wynik != 0)
			//printf("rozny od 0 dla T = %i, i0=%i, w v=%i\n", T, i0, v);
	}
	//printf("ile dla T=%i, i0=%i to %i\n", T, i0, wynik);
	return wynik;
}

int main()
{
	int n;
	long long int x;
	scanf("%i", &n);
	M = potega_dwojki(n);
	int rozmiar_drzewa = M * 2;
	przedzialowe *drzewo = new przedzialowe[rozmiar_drzewa];

	/* Inicjalizacja drzewa */

	for (int i = 0; i < rozmiar_drzewa; i++) {
		drzewo[i].push_back(std::make_pair(-1, 0));
	}
	//printf("M=%i\n", M);
	/*Akcja!*/
	x = 0;
	for (int T = 0; T < n; T++) {
		long long int i = 1 + ((x + T) * (x + T) * (x + T) % n);
		long long int j = i + ((x + T) * (x + T) * (x + T) % (n - i + 1));
		long long int c = ((x + T) * (x + T) * (x + T) % 10);
		long long int i_0 = 1 + ((x + 1 + T) * (x + 1 + T) * (x + 1 + T) % n);
		long long int t = (x + 1234567) % (T + 1);
		//printf("T=%i. ZWIEKSZ i=%i, j=%i, c=%i, PYTANIE i_0=%i, t=%i\n",T, i, j, c, i_0, t);
		if (c != 0) {
			wstaw(i, j, c, T + 1, drzewo);
		}
		x = ile(t, i_0, drzewo);
	}
	/* Wypisywanie wyjscia */
	for (int i = 1; i <= n; i++) {
		printf("%i\n", ile(n, i, drzewo));
	}

	/* Czyszczenie pamieci */
	usun(drzewo);
	return 0;
}

